package ito.poo.clases;

import java.time.LocalDate;

public class Lote {
	
		private static Object Fecha;
		private int numeroLote;
		private int numeroPz;
		private LocalDate fecha;
		/********************/
		public Lote() {
			super();
		}
		public Lote(int numeroLote, int numPz, LocalDate fecha) {
			super();
			this.numeroLote = numeroLote;
			this.numeroPz = numPz;
			this.fecha = fecha;
		}
		/********************/
		public int getNumeroLote() {
			return numeroLote;
		}

		public void setNumeroLote(int numeroLote) {
			this.numeroLote = numeroLote;
		}

		public int getNumeroPz() {
			return numeroPz;
		}

		public void setNumeroPz(int numeroPz) {
			this.numeroPz = numeroPz;
		}

		public LocalDate getFecha() {
			return fecha;
		}

		public void setFecha(LocalDate fecha) {
			this.fecha = fecha;
		}
		/********************/
		public float costoProducc(Prendas e) {
			float i=0;
			i = e.getCostoProducc() * this.numeroPz;
			return i;
		}

		public float montoProducxlote(Prendas e) {
			float i=0;
			i = costoProducc(e) * 0.15F;
			return i;
		}

		public float montoRecupxpieza(Prendas e) {
			float i=0;
			i = e.getCostoProducc() * 0.5F;
			return i;
		}
		/********************/
		@Override
		public String toString() {
			return "(Numero de lote: " + numeroLote + ", Numero de piezas: " + numeroPz + ", Fecha: " + fecha + ")";
		}
		/********************/
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
			result = prime * result + numeroLote;
			result = prime * result + numeroPz;
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Lote other = (Lote) obj;
			if (fecha == null) {
				if (other.fecha != null)
					return false;
			} else if (!fecha.equals(other.fecha))
				return false;
			if (numeroLote != other.numeroLote)
				return false;
			if (numeroPz != other.numeroPz)
				return false;
			return true;
		}
		public int compareTo(Lote arg0) {
			int r = 0;
			if (this.numeroLote != arg0.getNumeroLote())
				return this.numeroLote > arg0.getNumeroLote() ? 1 : -1;
			else if (this.numeroPz != arg0.getNumeroPz())
				return this.numeroPz > arg0.getNumeroPz() ? 2 : -2;
			else if (!Lote.Fecha.equals(arg0.getFecha()))
				return ((LocalDate) Lote.Fecha).compareTo(arg0.getFecha());
			return r;
		}

	}


