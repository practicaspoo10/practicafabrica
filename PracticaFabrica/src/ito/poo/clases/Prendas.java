package ito.poo.clases;

import java.time.LocalDate;
import java.util.ArrayList;

public class Prendas {
	

		private int model;
		private String tela;
		private float costoProducc;
		private String genero;
		private String temp;
		private ArrayList<Lote> lotes = new ArrayList<Lote>();
		/********************/
		public Prendas() {
			super();
		}
		
		public Prendas(int model, String genero, String temp, String tela, float costoProducc) {
			super();
			this.model = model;
			this.genero = genero;
			this.temp = temp;
			this.tela = tela;
			this.costoProducc = costoProducc;
		}
		/********************/
		public int getModel() {
			return model;
		}
		
		public void setModel(int model) {
			this.model = model;
		}

		public String getTela() {
			return tela;
		}

		public void setTela(String tela) {
			this.tela = tela;
		}

		public float getCostoProducc() {
			return costoProducc;
		}

		public void setCostoProducc(float costoProducc) {
			this.costoProducc = costoProducc;
		}

		public String getGenero() {
			return genero;
		}

		public void setGenero(String genero) {
			this.genero = genero;
		}
		public String getTemp() {
			return temp;
		}

		public void setTemp(String temp) {
			this.temp = temp;
		}

		/********************/
		public void addLote(int i, int j, LocalDate k) {
			Lote e = new Lote(i, j, k);
			this.lotes.add(e);
		}
		
		public Lote getLote(int i){
			Lote e = null;
			if (i > 1 || i < this.lotes.size())
				e = this.lotes.get(i - 1);
			return e;
		}
		/********************/
		public float costoxLote(float costoxUnidad) {
			float i = 0;
			return i;
		}
		/********************/
		@Override
		public String toString() {
			return "Prenda [model=" + model + ", genero=" + genero + ", temp=" + temp + ", "
					+ "tela=" + tela + ", costoProducc=" + costoProducc + " lotes=" + lotes + "]";
		}
		/********************/
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + Float.floatToIntBits(costoProducc);
			result = prime * result + ((genero == null) ? 0 : genero.hashCode());
			result = prime * result + ((lotes == null) ? 0 : lotes.hashCode());
			result = prime * result + model;
			result = prime * result + ((tela == null) ? 0 : tela.hashCode());
			result = prime * result + ((temp == null) ? 0 : temp.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Prendas other = (Prendas) obj;
			if (Float.floatToIntBits(costoProducc) != Float.floatToIntBits(other.costoProducc))
				return false;
			if (genero == null) {
				if (other.genero != null)
					return false;
			} else if (!genero.equals(other.genero))
				return false;
			if (lotes == null) {
				if (other.lotes != null)
					return false;
			} else if (!lotes.equals(other.lotes))
				return false;
			if (model != other.model)
				return false;
			if (tela == null) {
				if (other.tela != null)
					return false;
			} else if (!tela.equals(other.tela))
				return false;
			if (temp == null) {
				if (other.temp != null)
					return false;
			} else if (!temp.equals(other.temp))
				return false;
			return true;
		}
		public int compareTo(Prendas arg0) {
			int r=0;
			if (this.model!=((Prendas) arg0).getModel())
				return  this.model>((Prendas) arg0).getModel()? 1:-1;
			else if (!this.genero.equals(arg0.getGenero()))
				return this.genero.compareTo(arg0.getGenero());
			else if (!this.temp.equals(arg0.getTemp()))
				return this.temp.compareTo(arg0.getTemp());
			else if (!this.tela.equals(arg0.getTela()))
				return this.tela.compareTo(arg0.getTela());
			return r;
		}

}
