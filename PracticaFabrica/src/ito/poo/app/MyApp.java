package ito.poo.app;

import java.time.LocalDate;
import ito.poo.clases.Lote;
import ito.poo.clases.Prendas;
@SuppressWarnings("unused")

public class MyApp {
				
		static void run() {
		Prendas pre1 = new Prendas(102,"Femenino", "Primavera","Poliester", 165.98F);
		System.out.println(pre1);
		pre1.addLote(1, 2300, LocalDate.now());
		pre1.addLote(2, 5200, LocalDate.of(2021, 12, 10));
		System.out.println();
		System.out.println(pre1);
					
		System.out.println();
		System.out.println(pre1.getLote(2));
					
		System.out.println();
		System.out.println(pre1.getLote(1).costoProducc(pre1));
		System.out.println(pre1.getLote(2).costoProducc(pre1));
					
		System.out.println();
		System.out.println(pre1.getLote(1).montoProducxlote(pre1));
		System.out.println(pre1.getLote(1).montoRecupxpieza(pre1));
		
		System.out.println();
		System.out.println("/*******************/");
		System.out.println();

		Prendas pre2 = new Prendas(102,"Masculino", "Primavera","Poliester", 62.50F);
		System.out.println(pre2);
		pre2.addLote(3, 6400, LocalDate.now());
		pre2.addLote(4, 4100, LocalDate.of(2021, 12, 11));
		System.out.println();
		System.out.println(pre2);
					
		System.out.println();
		System.out.println(pre2.getLote(2));
					
		System.out.println();
		System.out.println(pre2.getLote(1).costoProducc(pre2));
		System.out.println(pre2.getLote(2).costoProducc(pre2));
					
		System.out.println();
		System.out.println(pre2.getLote(1).montoProducxlote(pre2));
		System.out.println(pre2.getLote(1).montoRecupxpieza(pre2));
		System.out.println();
		
		System.out.println(!pre1.equals(pre2));
		System.out.println(pre2.compareTo(pre1));
		}

		public static void main(String[] args) {
			run();
		}
	}

	
